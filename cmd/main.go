package main

import (
	"fmt"
	"time"

	"gitlab.com/cast_iron_runner/public/fmttypes"
)

func main() {
	// приводим комментарий к общей структуре
	comment := fmttypes.UnifiedComment{
		IP:        "",
		RatePlus:  6,
		RateMinus: 0,
		RateTotal: 6,
		CreatedAt: time.Now(),
		ID:        1337,
		Body:      "",
		ParentID:  15,
		Author: fmttypes.UnifiedAuthor{
			Name:      "Unknown Person",
			ID:        228,
			IP:        "",
			AvatarURL: "https://vk.com/photo123",
			UserBalls: 15,
		},
		SourceURL: "https://vk.com/post123",
	}

	// приводим пост к общей структуре
	post := fmttypes.UnifiedPost{
		URL:  "https://vk.com/post123",
		Type: "vk_post",
		Text: "Hello world",
	}

	// создаем ответ функции
	responseBytes, err := fmttypes.MakeResponse(
		"test-project-id",
		post,
		comment,
	)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", responseBytes)
}
