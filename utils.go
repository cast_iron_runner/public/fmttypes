package fmttypes

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"strconv"
)

// GetCommentHash возвращает SHA1 хеш комментария.
// Считает хеш по полям Comment.ID, Comment.Author.ID,
// Comment.SourceURL, Comment.Body, Comment.CreatedAt
func getCommentHash(comment UnifiedComment) string {
	h := sha1.New()
	h.Write([]byte(strconv.Itoa(comment.ID)))
	h.Write([]byte(strconv.Itoa(comment.Author.ID)))
	h.Write([]byte(comment.SourceURL))
	h.Write([]byte(comment.Body))
	h.Write([]byte(comment.CreatedAt.String()))
	return hex.EncodeToString(h.Sum(nil))
}

// GetPostHash возвращает SHA1 хеш поста.
// Считает хеш по полям Post.URL, Post.Type, Post.Text
func getPostHash(post UnifiedPost) string {
	h := sha1.New()
	h.Write([]byte(post.URL))
	h.Write([]byte(post.Type))
	h.Write([]byte(post.Text))
	return hex.EncodeToString(h.Sum(nil))
}

// MakeResponse создает ответ экстрактора
func MakeResponse(projectID string, post UnifiedPost, comments ...UnifiedComment) ([]byte, error) {
	dbPost := DBPost{
		UnifiedPost: post,
		ProjectID:   projectID,
		Hash:        getPostHash(post),
	}
	dbComments := make([]DBComment, len(comments))

	for i, comment := range comments {
		dbComments[i] = DBComment{
			UnifiedComment: comment,
			ProjectID:      projectID,
			Hash:           getCommentHash(comment),
		}
	}

	resp := UnifiedResponse{
		Post:     dbPost,
		Comments: dbComments,
	}

	return json.Marshal(resp)
}
