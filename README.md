## Как использовать
```go
package main

import (
	"fmt"
	"time"

	"gitlab.com/cast_iron_runner/public/fmttypes"
)

func main() {
	// приводим комментарий к общей структуре
	comment := fmttypes.UnifiedComment{
		IP:        "",
		RatePlus:  6,
		RateMinus: 0,
		RateTotal: 6,
		CreatedAt: time.Now(),
		ID:        1337,
		Body:      "",
		ParentID:  15,
		Author: fmttypes.UnifiedAuthor{
			Name:      "Unknown Person",
			ID:        228,
			IP:        "",
			AvatarURL: "https://vk.com/photo123",
			UserBalls: 15,
		},
		SourceURL: "https://vk.com/post123",
	}

	// приводим пост к общей структуре
	post := fmttypes.UnifiedPost{
		URL:  "https://vk.com/post123",
		Type: "vk_post",
		Text: "Hello world",
	}

	// создаем ответ функции
	responseBytes, err := fmttypes.MakeResponse(
		"test-project-id",
		post,
		comment,
	)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", responseBytes)
}

```

## Вывод (responseBytes)
```json
{ 
   "post":{ 
      "url":"https://vk.com/post123",
      "type":"vk_post",
      "text":"Hello world",
      "project_id":"test-project-id",
      "hash":"472e52d1ed699fa2d00f7f2f7252610b05a68d7b"
   },
   "comments":[ 
      { 
         "ip":"",
         "rate_plus":6,
         "rate_minus":0,
         "rate_total":6,
         "created_at":"2019-11-12T06:56:59.869459236+05:00",
         "id":1337,
         "body":"",
         "parent_id":15,
         "author":{ 
            "name":"Unknown Person",
            "id":228,
            "ip":"",
            "avatar_url":"https://vk.com/photo123",
            "user_balls":15
         },
         "source_url":"https://vk.com/post123",
         "project_id":"test-project-id",
         "hash":"b93770b27620c8832257b4e926bed2def4d951a3"
      }
   ]
}
```

P.S. Прокинуть массив комментариев в MakeResponse можно так
```go
	comments := make([]fmttypes.UnifiedComment, 20)
	responseBytes, err := fmttypes.MakeResponse(
		"project-id",
		post,
		comments..., // <-----
	)
```