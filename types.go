package fmttypes

import "time"

// UnifiedComment струкутра коммента
type UnifiedComment struct {
	IP              string        `json:"ip"`         // IP с которого был оставлен коммент
	RatePlus        int           `json:"rate_plus"`  // Лайки
	RateMinus       int           `json:"rate_minus"` // Дизы
	RateTotal       int           `json:"rate_total"` // Сумма лайков и дизов
	CreatedAt       time.Time     `json:"created_at"` // Время создания коммента
	ID              int           `json:"id"`         // ID коммента
	Body            string        `json:"body"`       // Текст коммента
	ParentID        int           `json:"parent_id"`  // ID коммента, в ответ которому был создан текущий
	Author          UnifiedAuthor `json:"author"`     // Автор коммента
	SourceURL       string        `json:"source_url"` // Ссылка на источник, откуда был взят коммент
	NormalizedWords []string      `json:"normalized_words"`
	ToxicScore      float32       `json:"toxic_score"`
	SantimentScore  struct {
		Negative float32 `json:"negative"`
		Skip     float32 `json:"skip"`
		Neutral  float32 `json:"neutral"`
		Speech   float32 `json:"speech"`
		Positive float32 `json:"positive"`
	} `json:"santiment_score"`
}

// UnifiedAuthor структура автора коммента
type UnifiedAuthor struct {
	Name      string `json:"name"`       // Имя автора
	ID        int    `json:"id"`         // ID автора
	IP        string `json:"ip"`         // IP автора
	AvatarURL string `json:"avatar_url"` // Ссылка на аватарку автора
	UserBalls int    `json:"user_balls"` // Баллы автора
}

// UnifiedPost структура поста
type UnifiedPost struct {
	URL  string `json:"url"`  // Ссылка на источник
	Type string `json:"type"` // Тип источника
	Text string `json:"text"` // Содержимое поста
}

// DBComment структура комментария, хранящаяся в БД
type DBComment struct {
	UnifiedComment
	ProjectID  string    `json:"project_id"` // ID проекта
	Hash       string    `json:"hash"`       // Хеш коммента
	InsertedAt time.Time `json:"inserted_at"`
	GeoInfo    Geo       `json:"geo_info"`
}

// DBPost структура поста, хранящаяся в БД
type DBPost struct {
	UnifiedPost
	ProjectID  string    `json:"project_id"` // ID проекта
	Hash       string    `json:"hash"`       // Хеш коммента
	InsertedAt time.Time `json:"inserted_at"`
}

// UnifiedResponse структура ответа экстрактора
type UnifiedResponse struct {
	Post     DBPost      `json:"post"`
	Comments []DBComment `json:"comments"`
}

// Geo структура информации, которая получается по ip
type Geo struct {
	City        string `json:"city"`
	Country     string `json:"contry"`
	TimeZome    string `json:"timezone"`
	Coordinates struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"coordinates"`
}
